//! An `include_bytes!`-like macro for including resource directories
//!
//! This module provides a single macro, `include_dir!`, which will traverse the given directory
//! recursively, packaging the files into the binary (like `include_bytes!`). The macro expands
//! into a static array of tuples of type `&'static [(&'static str, &'static [u8])]`, with the tuples
//! representing the mapping of the filepath (relative to the given directory path) to the file's
//! bytes. The file path (the first member of the tuple), is a unix-like path, which makes it easy
//! to use in a web context.
//! 
//! The macro is implemented as a compiler plugin. With the current state of compiler plugin support,
//! this means that rust nightly is required to use the macro.
//! 
//! The compiler will get confused if you do not declare the expected type, so be sure to include:
//! `&'static [(&'static str, &'static [u8])]` in the declaration!
//! 
//! ## Simple usage
//! ```
//! #![feature(plugin)]
//! #![plugin(include_dir)]
//! 
//! static RESOURCES: &'static [(&'static str, &'static [u8])] = include_dir!("../foo/bar");
//! 
//! fn get_res(key: &str) -> Option<&'static [u8]> {
//!     match RESOURCES.iter().find(|&&(k, _)| key == k) {
//!         Some(b) => Some(b.1),
//!         None => None
//!     }
//! }
//! 
//! fn main() {
//!     match get_res("index.html") {
//!         Some(f) => println!("{}", f),
//!         None => println!("no index.html packaged")
//!     };
//!     match get_res("assets/style.css") {
//!         Some(f) => println!("{}", f),
//!         None => println!("no assets/style.css packaged")
//!     };
//! }
//! ```
#![crate_type = "dylib"]
#![feature(plugin_registrar, rustc_private)]

extern crate rustc;
extern crate rustc_data_structures;
extern crate rustc_plugin;
extern crate syntax;
extern crate syntax_pos;

use rustc_plugin::Registry;
use rustc_data_structures::sync::Lrc;
use syntax_pos::{FileName, Span};
use syntax_pos::symbol::Symbol;
use syntax::ast;
use syntax::ext::base::*;
use syntax::ext::build::AstBuilder;
use syntax::ptr::P;
use syntax::tokenstream::TokenTree;

use std::io;
use std::io::prelude::*;
use std::fs;
use std::fs::{DirEntry, File};
use std::path::{Path, PathBuf};

pub fn expand_include_dir(
    cx: &mut ExtCtxt,
    sp: Span,
    tts: &[TokenTree],
) -> Box<MacResult + 'static> {
    // extract the given path to the directory
    let dir_str = match get_single_str_from_tts(cx, sp, tts, "include_dir!") {
        None => return DummyResult::expr(sp),
        Some(f) => f,
    };
    // the path should be relative to the callsite
    let dir = resolve_path(cx, sp, dir_str);
    if !dir.exists() {
        cx.span_err(sp, &format!("{} does not exist", dir.display()));
        return DummyResult::expr(sp);
    }
    if !dir.is_dir() {
        cx.span_err(sp, &format!("{} is not a directory", dir.display()));
        return DummyResult::expr(sp);
    }
    let dir_path = dir.as_path();
    let mut tuples: Vec<P<ast::Expr>> = Vec::new();
    walk_simple(dir_path, &mut |f: &DirEntry| {
        let pb = f.path().to_path_buf();
        let mut bytes = Vec::new();
        let outbytes = match File::open(&pb).and_then(|mut i| i.read_to_end(&mut bytes)) {
            Err(e) => {
                cx.span_fatal(sp, &format!("couldn't read {}: {}", pb.display(), e));
            }
            Ok(..) => {
                // track files so we know to recompile when they change
                cx.codemap().new_filemap_and_lines(&pb, "");
                bytes
            }
        };
        let access_path = pb.clone();
        let fixed_path = match access_path.strip_prefix(dir_path) {
            Ok(p) => p.to_str(),
            Err(e) => cx.span_fatal(sp, &format!("couldn't strip {} from {}: {}", dir_path.display(), access_path.display(), e))
        }.unwrap();
        let path_expr = cx.expr_lit(sp, ast::LitKind::Str(Symbol::intern(fixed_path), ast::StrStyle::Cooked));
        let byte_expr = cx.expr_lit(sp, ast::LitKind::ByteStr(Lrc::new(outbytes)));
        tuples.push(cx.expr_tuple(sp, vec![path_expr, byte_expr]));
    }).unwrap();
    MacEager::expr(cx.expr_vec_slice(sp, tuples))
}

fn resolve_path(cx: &mut ExtCtxt, sp: Span, arg: String) -> PathBuf {
    let arg = PathBuf::from(arg);

    if !arg.is_absolute() {
        let callsite = sp.source_callsite();
        let mut path = match cx.codemap().span_to_unmapped_path(callsite) {
            FileName::Real(path) => path,
            other => cx.span_fatal(sp, &format!("cannot resolve relative path: `{}`", other))
        };
        path.pop();
        path.push(arg);
        path
    } else {
        arg
    }
}

fn walk_simple(dir: &Path, cb: &mut FnMut(&DirEntry)) -> io::Result<()> {
    if dir.is_dir() {
        for entry in fs::read_dir(dir)? {
            let entry = entry?;
            let path = entry.path();
            if path.is_dir() {
                walk_simple(&path, cb)?;
            } else {
                cb(&entry);
            }
        }
    }
    Ok(())
}

#[plugin_registrar]
pub fn plugin_registrar(reg: &mut Registry) {
    reg.register_macro("include_dir", expand_include_dir);
}
