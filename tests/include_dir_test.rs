#![feature(plugin)]
#![plugin(include_dir)]

static RESOURCES: &'static [(&'static str, &'static [u8])] = include_dir!("../src");

#[test]
fn did_include() {
    assert_eq!(RESOURCES[0].0, "lib.rs");
    let src_bytes = get_res("lib.rs").unwrap();
    assert_ne!(src_bytes.len(), 0);
    let src_str = std::str::from_utf8(src_bytes).unwrap();
    assert!(src_str.contains("expand_include_dir"));
}

fn get_res(key: &str) -> Option<&'static [u8]> {
    match RESOURCES.iter().find(|&&(k, _)| key == k) {
        Some(b) => return Some(b.1),
        None => return None
    }
}
